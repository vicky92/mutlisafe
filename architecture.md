# Architecture

Container driven architecture to manage dependencies in one place.

Below is the directory structure.

```
my-app
├── index.js // entry point
├── .env // environment variable template
├── Readme.md
├── package.json
├── node_modules
├── .gitignore
├── tests // test suite
│   └── app.spec.js
└── src
│    ├── controllers
│    │   └── index.js
│    ├── config
│    │   └── index.js // main config, loads from env and exports final configs
│    ├── services
│    │   └── index.js
│    ├── utils
│    │   └── createController.js // utility to require and make controllers.
│    ├── container.js // container, imports all dependencies and exports a container.
│    ├── containerHelper.js // To inject dependencies.
│    ├── router.js // Main router, creates controllers and connects to express router
│    └── server.js // express js app.
```

