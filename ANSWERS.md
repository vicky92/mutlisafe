######

Commenting w.r.t Master - Slave Architecture. CAP THEOREM.

1. What are the tradeoffs when it comes to optimizing for availability vs consistency?
A: If we goes for availibilty, then consistency might be comprised as it taked time to sync with other data sources.
   If we goes for consistency, then availibilty might be sacrificed as some request can be timeout which leads to data availibity. 

######
2. In a real world scenario, if you only had to choose between availability and consistency, which criteria would you pick and why?
A: Availabity is essential when data collection is priority, if we want to capture user behaviour data or its preference, then we can go for consistency.
   Consistency is essentail when same data should be consistent across multiple users or vendors. 
   It all depends on the context and the product behaviour.

######
2. Is it possible to get both - a highly available and highly consistent data all the time? If no, then why? If yes, then how?
A: We can achieve the CA with AWS aurora. It handle partition tolerance, provide consistency(latency of 20 ms or 20 sec based on configuration) and availibity.
   Currently, to manage inhours infra its very tedious and we need to be on foot 24 * 7 if something went wrong. 
   AWS Cloud handle all our headaches and we can focus on product development rather than on infra.     