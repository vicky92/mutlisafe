const awilix = require('awilix');
const server = require('@src/server');
const routes = require('@src/router');
const logger = require('@src/utils/logger');
const axiosWrapper = require('@src/utils/axiosWrapper');
const response = require('@src/utils/response');
const cache = require('@src/utils/cache');
const config = require('@src/config');

const { createContainer, asValue, asFunction, Lifetime } = awilix;

const container = createContainer();

// SYSTEM
container.register({
  server: asFunction(server).singleton(),
  router: asFunction(routes).singleton(),
  logger: asFunction(logger).singleton(),
  axiosWrapper: asFunction(axiosWrapper).singleton(),
  response: asFunction(response).singleton(),
  cache: asFunction(cache).singleton(),
  config: asValue(config),
});

container.loadModules(['services/*.js'], {
  resolverOptions: {
    register: asFunction,
    lifetime: Lifetime.SINGLETON,
  },
  cwd: __dirname,
});

module.exports = container;
