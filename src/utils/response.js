const { assoc, merge } = require('ramda');

module.exports = ({ logger }) => {
  const defaultResponse = (success = true) => {
    return {
      success,
      version: 'V1',
      date: new Date(),
    };
  };

  const Success = async (data, doTranslate, language) => {
    let translatedData = data;
    if (doTranslate) {
      translatedData = await _getTranslatedText(data, language);
    }
    return assoc('data', translatedData, defaultResponse(true));
  };

  const Fail = async (data, language) => {
    const translatedData = await _getTranslatedText(data, language);
    return assoc('error', translatedData, defaultResponse(false));
  };

  const _getTranslatedText = async (data, language) => {
    const langAttributes = await localization.getTranslatedText(language);
    const engLangAttributes = await localization.getTranslatedText('en_US');
    const mergeTranslations = merge(engLangAttributes, langAttributes);
    const jsonToString = JSON.stringify(data);

    let interpolationString;
    try {
      interpolationString = format(jsonToString, mergeTranslations);
      interpolationString = interpolationString.replace(/\s\s+/g, ' ');
    } catch (error) {
      interpolationString = format(jsonToString, engLangAttributes);
      interpolationString = interpolationString.replace(/\s\s+/g, ' ');
      logger.error(`Translation error in ${language} : ${data}`, error);
    }

    return JSON.parse(interpolationString);
  };

  return {
    Success,
    Fail,
  };
};
