const axios = require('axios');
module.exports = ({ logger }) => {
  const getRequest = async (url, params = {}) => {
    try {
      const response = await axios.get(url, params, {
        headers: {
          'User-Agent': 'PostmanRuntime/7.26.8',
        },
      });
      logger.info(`Response :: ${JSON.stringify(response.data)}`);
      return response.data;
    } catch (error) {
      logger.error(`Error while calling api`, error);
      throw error;
    }
  };

  return {
    getRequest,
  };
};
