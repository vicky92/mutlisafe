const Redis = require('ioredis');
const redis = new Redis();

module.exports = ({ logger }) => {
  const set = async (cacheName, key, value, expire) => {
    try {
      await redis.set(`${cacheName}_${key}`, value, 'ex', expire);
    } catch (error) {
      logger.error(`Error while adding key in redis`, error);
    }
  };

  const get = async (cacheName, key) => {
    try {
      let result = await redis.get(`${cacheName}_${key}`);
      return result;
    } catch (error) {
      logger.error(`Error while extracting key from redis`, error);
    }
  };

  const quit = async () => {
    await redis.quit();
  };

  return {
    set,
    get,
    quit,
  };
};
