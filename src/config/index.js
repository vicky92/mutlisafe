require('dotenv').config();

module.exports = {
  PORT: process.env.PORT,
  GNOSIS_ENDPOINT: process.env.GNOSIS_ENDPOINT,
};
