const HttpError = require('@src/utils/httpError');
const express = require('express');
const Status = require('http-status')
const router = express.Router();
const container = require('@src/container');

const {
  balanceService,
  logger,
  response: { Success },
} = container.cradle;
module.exports = () => {
  router.get('/balance', async (req, res, next) => {
    try {
      let addresses = req.query;
      let response = await balanceService.getBalance(addresses);
      res.status(Status.OK).json(await Success(response));
    } catch (error) {
      logger.error(error.message);
      next(new HttpError(error));
    }
  });

  return router;
};
