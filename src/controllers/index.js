const HttpError = require('@src/utils/httpError');
const express = require('express');
const router = express.Router();
const container = require('@src/container');

const { logger } = container.cradle;
module.exports = () => {
  router.get('/', async (req, res, next) => {
    try {
      res.json({ test: 'hello world' });
    } catch (error) {
      logger.error(error.message);
      next(new HttpError(error));
    }
  });

  return router;
};
