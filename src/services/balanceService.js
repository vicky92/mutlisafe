import { define } from '../containerHelper'

module.exports = define('balanceService',({ logger, axiosWrapper, cache, config }) => {
  const getBalance = async (body) => {
    let { addresses } = body
    let responseArr = [];
    addresses = addresses.split(',')
    let leftOutAddresses = []
    while(addresses.length) {
        let address = addresses[0]
        let apiResponse = await _getCache(address)
        if(apiResponse) {
            logger.debug(`Cache found for address :: ${address}`)
            responseArr.push({ address, apiResponse });
        } else {
            leftOutAddresses.push(address)
        }
        addresses.splice(0, 1)
    }
    await Promise.all(leftOutAddresses.map(async address => {
        logger.debug(`Api Calling for address :: ${address}`)
        let url = `${config.GNOSIS_ENDPOINT}safes/${address}/balances/usd/?trusted=false&exclude_spam=false`;
        let apiResponse = await axiosWrapper.getRequest(url);
        await _setCache(address, apiResponse);
        responseArr.push({ address, apiResponse });
    }))
    return responseArr;   
  };

  const _setCache = async (address, response) => {
    await cache.set('BALANCE', address, JSON.stringify(response), 60 * 60)
  }

  const _getCache = async(address) => {
    let cacheValue = await cache.get('BALANCE', address)
    return JSON.parse(cacheValue)
  }

  return {
    getBalance,
  };
});