require('@babel/register');

const request = require('supertest');
const container = require('@src/container');

describe('Balance check', () => {
  const { server, cache } = container.cradle;
  const app = request(server.app);

  it('should return OK.', async () => {
    let addresses = [
      '0x5e14ed9dCeE22ba758E8de482301028b261c4a14',
      '0xF1D8c2eED95D5fC2EaDe4E6Bb15a5969453E89a9',
      '0x89C51828427F70D77875C6747759fB17Ba10Ceb0',
      '0xf26d1Bb347a59F6C283C53156519cC1B1ABacA51',
    ].join(',');
    const response = await app.get(`/api/v1/balance?addresses=${addresses}`);
    expect(response.statusCode).toBe(200);
  });

  afterAll(async () => {
    await cache.quit();
  });
});
