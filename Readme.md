# Node Boiler Plate for sequelize
 
nodejs boilerplate with redis only for api development

# Technologies:

- Node.js & Express.js
- Redis

# Getting Started

## Local Development

Ensure redis is installed in your machine, then run:

```bash
cp .env.example .env
# Edit credentials in .env
npm install
npm dev
```

## Deployment with PM2

- `pm2 start`

## Test Development

- `npm run test`

# Coding standards

- Prettier
- Eslint
